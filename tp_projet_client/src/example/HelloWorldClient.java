package example;

import fr.univ.smb.sirambd.trouvetontrain.HelloWorldService;
import fr.univ.smb.sirambd.trouvetontrain.MainService;

public class HelloWorldClient {
  public static void main(String[] argv) {
      fr.univ.smb.sirambd.trouvetontrain.HelloWorld service = new HelloWorldService().getHelloWorldPort();
      fr.univ.smb.sirambd.trouvetontrain.Main serveMain = new MainService().getMainPort();
      //invoke business method
      System.out.println(service.sayHelloWorldFrom("sirambd"));
      System.out.println( "distance est : " + serveMain.getDistance(5.924991, 45.563812, 5.915897, 45.691039) + " km");
      System.out.println( "prix est : " + serveMain.getDistance(5.924991, 45.563812, 5.915897, 45.691039) + " euros");
  }
}
