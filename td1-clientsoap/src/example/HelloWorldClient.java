package example;

import fr.univ.smb.mypackage.HelloWorldService;
import fr.univ.smb.mypackage.*;

public class HelloWorldClient {
  public static void main(String[] argv) {
    HelloWorld service = new HelloWorldService().getHelloWorldPort();
    //invoke business method
    System.out.println(service.sayHelloWorldFrom("sirambd"));
    System.out.println(service.addition(2, 5));
  }
}