<nav class="navbar navbar-expand-md bg-white navbar-light fixed-top">
    <!-- Brand -->
    <a class="navbar-brand" href="#">
        <img src="images/ttt.png" alt="logo">
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ml-md-auto bg-white bg-orange bg-md-white" id="navbarnav">
            <li class="nav-item">
                <a class="nav-link" href="#">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">A propos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Nous contacter</a>
            </li>
        </ul>
    </div>
</nav>