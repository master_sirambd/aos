package fr.univ.smb.sirambd.servlets;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fr.univ.smb.sirambd.helpers.JsonReader;
import fr.univ.smb.sirambd.wsdl.TrouveTonTrain;
import fr.univ.smb.sirambd.wsdl.TrouveTonTrainService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet(name = "ArretServlet")
public class ArretServlet extends HttpServlet {
    private final String
            url = "https://api.sncf.com/v1/coverage/sncf/journeys?count=10&",
            user = "09d6b8b7-ac2c-4bf7-9985-3d4d8ce028e3",
            pass = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String date = request.getParameter("mydate");
        String departCoord = request.getParameter("depart");
        String arriveeCoord = request.getParameter("arrivee");
        String[] departCoordArray = departCoord.split(";"); // 0:lon; 1:lat; 2:nom
        String[] arriveeCoordArray = arriveeCoord.split(";"); // 0:lon; 1:lat
        String nomDepart = departCoordArray[2]; // Nom de départ
        String nomArrivee = arriveeCoordArray[2]; // Nom arrivée
        Double departLon = Double.valueOf(departCoordArray[0]);
        Double arriveeLon = Double.valueOf(arriveeCoordArray[0]);
        Double departLat = Double.valueOf(departCoordArray[1]);
        Double arriveeLat = Double.valueOf(arriveeCoordArray[1]);
        String from = "from=" + departCoord + "&";
        String to = "to=" + arriveeCoord + "&";

        System.out.println("depart lon " + departLon);
        System.out.println("depart lat " + departLat);
        System.out.println("arrivee lon " + arriveeLon);
        System.out.println("arrivee lat " + arriveeLat);

        JsonArray resArray = new JsonArray();
        TrouveTonTrain service = new TrouveTonTrainService().getTrouveTonTrainPort(); // Service

        Double distance = service.getDistance(departLon, departLat, arriveeLon, arriveeLat);
        Double prix = service.getPrix(departLon, departLat, arriveeLon, arriveeLat);
        try {
            JsonObject json = JsonReader.readJsonFromUrl(url + from + to, user, pass).getAsJsonObject();
            JsonArray journeys = json.getAsJsonArray("journeys");


            for (JsonElement elm : journeys){
                JsonObject jo = new JsonObject();
                JsonObject current = elm.getAsJsonObject();
                JsonArray sections = current.getAsJsonArray("sections");
                String[] tDepArr = current.get("departure_date_time").getAsString().split("T");
                String[] tArrivArr = current.get("arrival_date_time").getAsString().split("T");
                String tDep = tDepArr[1].substring(0,2) + "h" + tDepArr[1].substring(2,4);
                String tArr = tArrivArr[1].substring(0,2) + "h" + tArrivArr[1].substring(2,4);

                jo.addProperty("depart", nomDepart);
                jo.addProperty("arrivee", nomArrivee);
                jo.addProperty("heurre_depart", tDep);
                jo.addProperty("heurre_arrivee", tArr);
                jo.addProperty("distance", distance + "km");
                jo.addProperty("prix", prix + "€");
                resArray.add(jo);
            }

            //out.println(resArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("arrets", resArray);
        RequestDispatcher rd=request.getRequestDispatcher("/index.jsp");
        rd.include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
