package fr.univ.smb.sirambd.servlets;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fr.univ.smb.sirambd.helpers.JsonReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class RechercheArretServlet extends HttpServlet {
    private final String
                        url = "https://api.sncf.com/v1/coverage/sncf/places?q=",
                        user = "09d6b8b7-ac2c-4bf7-9985-3d4d8ce028e3",
                        pass = "";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
;
        String nom_arret = request.getParameter("name");

        JsonObject json = null;
        String string_json;
        try {
            json = JsonReader.readJsonFromUrl(url + nom_arret, user, pass).getAsJsonObject();
            string_json = JsonReader.toPrettyFormat(json);

            // result json
            JsonObject jo = new JsonObject();
            JsonArray ja = new JsonArray();
            jo.addProperty("sucess", "true");

            JsonArray places = json.get("places").getAsJsonArray();
            for (JsonElement jsonEl : places){
                JsonObject currentJsonObject = jsonEl.getAsJsonObject();
                JsonObject subjo = new JsonObject();
                String typeArret = currentJsonObject.get("embedded_type").getAsString();
                if (typeArret.equals("stop_area")){
                    subjo.addProperty("name", currentJsonObject.get("name").getAsString());
                    String lat = currentJsonObject.get("stop_area").getAsJsonObject().get("coord").getAsJsonObject().get("lat").getAsString();
                    String lon = currentJsonObject.get("stop_area").getAsJsonObject().get("coord").getAsJsonObject().get("lon").getAsString();
                    subjo.addProperty("value", lon + ";" + lat + ";" + nom_arret);
                    ja.add(subjo);
                }
            }
            jo.add("results", ja);

            out.println(jo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
