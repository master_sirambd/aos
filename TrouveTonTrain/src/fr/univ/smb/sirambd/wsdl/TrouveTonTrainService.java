
package fr.univ.smb.sirambd.wsdl;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "TrouveTonTrainService", targetNamespace = "http://trouvetontrain/", wsdlLocation = "http://trouve-ton-train-sirambd-service-soap.azurewebsites.net/services/TrouveTonTrain?wsdl")
public class TrouveTonTrainService
    extends Service
{

    private final static URL TROUVETONTRAINSERVICE_WSDL_LOCATION;
    private final static WebServiceException TROUVETONTRAINSERVICE_EXCEPTION;
    private final static QName TROUVETONTRAINSERVICE_QNAME = new QName("http://trouvetontrain/", "TrouveTonTrainService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://trouve-ton-train-sirambd-service-soap.azurewebsites.net/services/TrouveTonTrain?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        TROUVETONTRAINSERVICE_WSDL_LOCATION = url;
        TROUVETONTRAINSERVICE_EXCEPTION = e;
    }

    public TrouveTonTrainService() {
        super(__getWsdlLocation(), TROUVETONTRAINSERVICE_QNAME);
    }

    public TrouveTonTrainService(WebServiceFeature... features) {
        super(__getWsdlLocation(), TROUVETONTRAINSERVICE_QNAME, features);
    }

    public TrouveTonTrainService(URL wsdlLocation) {
        super(wsdlLocation, TROUVETONTRAINSERVICE_QNAME);
    }

    public TrouveTonTrainService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, TROUVETONTRAINSERVICE_QNAME, features);
    }

    public TrouveTonTrainService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TrouveTonTrainService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns TrouveTonTrain
     */
    @WebEndpoint(name = "TrouveTonTrainPort")
    public TrouveTonTrain getTrouveTonTrainPort() {
        return super.getPort(new QName("http://trouvetontrain/", "TrouveTonTrainPort"), TrouveTonTrain.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns TrouveTonTrain
     */
    @WebEndpoint(name = "TrouveTonTrainPort")
    public TrouveTonTrain getTrouveTonTrainPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://trouvetontrain/", "TrouveTonTrainPort"), TrouveTonTrain.class, features);
    }

    private static URL __getWsdlLocation() {
        if (TROUVETONTRAINSERVICE_EXCEPTION!= null) {
            throw TROUVETONTRAINSERVICE_EXCEPTION;
        }
        return TROUVETONTRAINSERVICE_WSDL_LOCATION;
    }

}
