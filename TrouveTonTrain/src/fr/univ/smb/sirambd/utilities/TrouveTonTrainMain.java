package fr.univ.smb.sirambd.utilities;

import fr.univ.smb.sirambd.wsdl.TrouveTonTrain;
import fr.univ.smb.sirambd.wsdl.TrouveTonTrainService;

public class TrouveTonTrainMain {
    public static void main(String[] args) {
        TrouveTonTrain service = new TrouveTonTrainService().getTrouveTonTrainPort();
        //invoke business method
        System.out.println("distance est : " + service.getDistance(5.924991, 45.563812, 5.915897, 45.691039) + " km");
        System.out.println("prix est : " + service.getPrix(5.924991, 45.563812, 5.915897, 45.691039) + " euros");
    }
}
