<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="mb-5">
    <h1 class="text-white">Vous cherchez une destination? <br/> Trouve Ton Train est là pour vous</h1>
    <div class="container mt-5">
        <form action="arrets" method="post" class="form">
            <div class="row justify-content-center">
                <div class="col-xl-3 col-md-4 mt-3">
                    <input type="date" name="mydate" id="date" class="form-control" required="required">
                </div>
                <div class="col-xl-3 col-md-4 mt-3">
                    <!--<input type="search" name="depart" id="depart" class="form-control" placeholder="Départ...">-->
                    <select name="depart" id="depart" class="ui search dropdown" required="required">
                        <option value="">Choisissez votre départ...</option>
                    </select>
                </div>
                <div class="col-xl-3 col-md-4 mt-3">
                    <!--<input type="search" name="arrivee" id="arrivee" class="form-control" placeholder="Destination...">-->
                    <select name="arrivee" id="arrivee" class="ui search dropdown" required="required">
                        <option value="">Choisissez votre arrivée...</option>
                    </select>
                </div>
            </div>

            <div class="row justify-content-center mt-3">
                <input type="submit" value="Rechercher" class="btn btn-orange">
            </div>
        </form>
    </div>
</header>