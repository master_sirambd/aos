package trouvetontrain;

import fr.univ.smb.sirambd.trouvetontrain.TrouveTonTrainService;

public class TrouveTonTrainClient {
    public static void main(String[] argv) {
        fr.univ.smb.sirambd.trouvetontrain.TrouveTonTrain service = new TrouveTonTrainService().getTrouveTonTrainPort();
        //invoke business method
        System.out.println( "distance est : " + service.getDistance(5.924991, 45.563812, 5.915897, 45.691039) + " km");
        System.out.println( "prix est : " + service.getPrix(5.924991, 45.563812, 5.915897, 45.691039) + " euros");
    }
}
