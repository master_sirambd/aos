<header class="mb-5">
    <h1 class="text-white">Vous cherchez une destination? <br/> Trouve Ton Train est là pour vous</h1>
    <div class="container mt-5">
        <form action="#" method="post" class="form">
            <div class="row justify-content-center">
                <div class="col-xl-3 col-md-4 mt-3">
                    <input type="date" name="mydate" id="date" class="form-control">
                </div>
                <div class="col-xl-3 col-md-4 mt-3">
                    <input type="search" name="depart" id="depart" class="form-control" placeholder="Départ...">
                </div>
                <div class="col-xl-3 col-md-4 mt-3">
                    <input type="search" name="arrivee" id="arrivee" class="form-control" placeholder="Destination...">
                </div>
            </div>

            <div class="row justify-content-center mt-3">
                <input type="submit" value="Rechercher" class="btn btn-orange">
            </div>
        </form>
    </div>
</header>