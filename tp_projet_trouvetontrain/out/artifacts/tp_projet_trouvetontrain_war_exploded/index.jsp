<%--
  Created by IntelliJ IDEA.
  User: babdo
  Date: 17/01/2019
  Time: 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Trouve Ton Train</title>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/app.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  <!-- nav -->
  <%@include file="partials/nav.jsp" %>

  <!-- header -->
  <%@include file="partials/header.jsp"%>

  <section class="container">
    <div class="row">
      <aside class="col-md-12">
        <div class="row">
          <div class="col-2">
            <p>
              <b>16h45</b> <br> 20h20
            </p>
          </div>
          <div class="col-8">
            <p>
              <b>Annecy</b>
              <br>
              Geneve
            </p>
          </div>
          <div class="col-2 text-right">
            <p>
              <b>6,70$</b>
            </p>
          </div>
        </div>
      </aside>
    </div>
  </section>

  <!-- footer -->
  <%@include file="partials/footer.jsp" %>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
