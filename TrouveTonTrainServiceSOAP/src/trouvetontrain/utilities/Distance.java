package trouvetontrain.utilities;

import trouvetontrain.models.Ville;

public class Distance {
    public static double rayon_terre = 6378137;
    public static double calcule_distance(Ville vA, Ville vB){
        double d_lambda = vB.getLongitude() - vA.getLongitude();
        double distance = Math.toRadians(Math.acos(Math.sin(vA.getLatitude()) *  Math.sin(vB.getLatitude()) + Math.cos(vA.getLatitude()) * Math.cos(vB.getLatitude()) * Math.cos(d_lambda)));
        return (distance * rayon_terre)/1000;
    }
}
