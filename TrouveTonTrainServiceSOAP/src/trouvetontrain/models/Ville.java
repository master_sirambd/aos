package trouvetontrain.models;

public class Ville {
    private double longitude;
    private double latitude;

    public Ville(double longitude, double latitude){
        this.latitude = longitude;
        this.longitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
