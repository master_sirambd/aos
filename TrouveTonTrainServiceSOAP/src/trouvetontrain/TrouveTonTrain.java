package trouvetontrain;

import trouvetontrain.models.Ville;
import trouvetontrain.utilities.Distance;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService()
public class TrouveTonTrain {
    @WebMethod
    public double getDistance(double longA, double latA, double longB, double latB){
        Ville a = new Ville(longA, latA);
        Ville b = new Ville(longB, latB);
        return Distance.calcule_distance(a, b);
    }

    @WebMethod
    public double getPrix(double longA, double latA, double longB, double latB){
        Ville a = new Ville(longA, latA);
        Ville b = new Ville(longB, latB);
        double d = Distance.calcule_distance(a, b);
        return d * 2;
    }
}
